class Tweet < ApplicationRecord
  belongs_to :user
  validates :body, length: { maximum: 180 }, allow_blank: false
  validate :no_repeated_tweets

  def no_repeated_tweets
    user_tweets = user.tweets.where(
      created_at: (1.day.ago..DateTime.now),
      body: body
    )
    if user_tweets.any?
      errors.add(:body, 'Cannot post repeat tweets in a single day')
    end
  end
end
