FactoryBot.define do
  factory :tweet do
    body { Faker::GreekPhilosophers.quote }
    user

    trait :long do
      body { Faker::String.random(length: 181) }
    end

    trait :recent do
      created_at { 1.hour.ago }
    end
  end

end
