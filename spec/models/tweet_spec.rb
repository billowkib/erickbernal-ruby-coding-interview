require 'rails_helper'

RSpec.describe Tweet, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"

  describe 'invalid tweet' do
    let(:long_tweet) { build(:tweet, :long) }
    it 'does not store long tweet' do
      long_tweet.save
      expect(long_tweet).not_to be_persisted
    end

    let!(:old_tweet) { create(:tweet) }
    let(:new_tweet) { build(:tweet, :recent, body: old_tweet.body, user: old_tweet.user)}
    it 'does not save repeated tweets' do
      new_tweet.save
      expect(new_tweet).not_to be_persisted
    end
  end
end
